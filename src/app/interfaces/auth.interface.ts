export interface RequestLogin {
  noHP: string;
  password: string;
}

export interface ResponseLogin {
  token: string;
  role: string;
}

export interface ResponseRegister {
  role: string;
}
