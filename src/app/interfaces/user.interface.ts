export interface User {
  id: number;
  name: string;
  phone: string;
  username: string;
  website: string;
  email: string;
  company: {
    bs: string;
    catchPhrase: string;
    name: string;
  };
  address: {
    city: string;
    street: string;
    suite: string;
    zipcode: string;
    geo: {
      lat: string;
      lng: string;
    };
  };
}
export interface UserList {
  id: number;
  nama: string;
  email: string;
  noHP: string;
  tanggalLahir: string;
  role: string;
  photo?: string;
}

export interface ResponseUploadPhoto {
  image: string;
}
