export interface KategoriList {
  id: number;
  namaKategori: string;
  gambarKategori?: string;
}
