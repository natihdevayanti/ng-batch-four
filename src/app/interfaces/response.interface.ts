// generic data biar bisa direuse
export interface ResponseBase<T> {
  message: string;
  data: T;
}
