import { Component, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  // title = 'ng-batch-four';
  // name = 'Natih';
  // age = 20;
  // status = false;
  // person = {
  //   title: 'Test A',
  //   name: 'ngroki',
  //   age: 0,
  //   status: true,
  // };
  // personList = [
  //   {
  //     title: 'Test A',
  //     name: 'ngroki',
  //     age: 0,
  //     status: true,
  //   },
  //   {
  //     title: 'Test B',
  //     name: 'ngroki',
  //     age: 0,
  //     status: false,
  //   },
  // ];
  // datas = [1, 2, 3];
  // // datas = new Array(10);
  // constructor() {
  //   this.name = 'Roki';
  //   this.age = 21;
  // }
  // onCallBack(ev: any) {
  //   console.log(ev);
  //   this.personList.push(ev.data);
  // }
}
