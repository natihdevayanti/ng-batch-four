import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { KategoriList } from '../interfaces/kategori.interface';
import { ResponseBase } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root',
})
export class KategoriService {
  private baseApi = 'http://localhost:8080';
  constructor(private httpClient: HttpClient) {}

  listKategori(): Observable<KategoriList[]> {
    // const token = localStorage.getItem('token');
    // const headers = new HttpHeaders({
    //   Authorization: `Bearer ${token}`,
    // });
    return this.httpClient
      .get<ResponseBase<KategoriList[]>>(`${this.baseApi}/lihat-kategori`)
      .pipe(
        map((val) => {
          const data: KategoriList[] = [];
          // mapping data for new response
          for (let item of val.data) {
            data.push({
              id: item.id,
              namaKategori: item.namaKategori,
              gambarKategori: `${this.baseApi}/files/${item.gambarKategori}:.+`,
            });
          }
          return data;
        }),
        catchError((error) => {
          console.log(error);
          throw error;
        })
      );
  }

  addKategori(payload: { namaKategori: string; gambarKategori?: string }) {
    return this.httpClient.post(this.baseApi + '/tambah-kategori', payload);
  }

  editKategori(
    payload: { namaKategori: string; gambarKategori?: string },
    id: number
  ) {
    return this.httpClient.put(this.baseApi + '/edit-kategori/' + id, payload);
  }

  deleteKategori(
    payload: { namaKategori: string; gambarKategori?: string },
    id: number
  ) {
    return this.httpClient.delete(this.baseApi + '/hapus-kategori/' + id);
  }

  uploadPhotoKategori(data: File): Observable<ResponseBase<string>> {
    // const token = localStorage.getItem('token');
    // const headers = new HttpHeaders({
    //   Authorization: `Bearer ${token}`,
    // });
    const file = new FormData();
    file.append('file', data, data.name);
    return this.httpClient.post<ResponseBase<string>>(
      `${this.baseApi}/upload-file`,
      file
    );
  }
}
