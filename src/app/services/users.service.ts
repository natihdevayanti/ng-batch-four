import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';
// import { ResponseBase } from '../interfaces/response.interface';
import { ResponseUploadPhoto, UserList } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  private baseApi = 'http://localhost:8080';
  constructor(private httpClient: HttpClient) {}

  listUsers(): Observable<UserList[]> {
    // const token = localStorage.getItem('token');
    // const headers = new HttpHeaders({
    //   Authorization: `Bearer ${token}`,
    // });
    return this.httpClient
      .get<ResponseBase<UserList[]>>(`${this.baseApi}/lihat-user`)
      .pipe(
        map((val) => {
          const data: UserList[] = [];
          // mapping data for new response
          for (let item of val.data) {
            data.push({
              id: item.id,
              nama: item.nama,
              email: item.email,
              noHP: item.noHP,
              tanggalLahir: item.tanggalLahir,
              role: item.role,
              photo: `${this.baseApi}/files/${item.photo}:.+`,
            });
          }
          return data;
        }),
        catchError((error) => {
          console.log(error);
          throw error;
        })
      );
  }

  private baseApiFile = 'https://1cfe-125-164-20-251.ap.ngrok.io';

  uploadPhoto(data: File): Observable<ResponseBase<string>> {
    // const token = localStorage.getItem('token');
    // const headers = new HttpHeaders({
    //   Authorization: `Bearer ${token}`,
    // });
    const file = new FormData();
    file.append('file', data, data.name);
    return this.httpClient.post<ResponseBase<string>>(
      `${this.baseApi}/upload-file`,
      file
      // { headers: headers }
    );
  }
  // kalau datanya array
  // listUsers(): Observable<ResponseBase<UserList[]>> {
  //   return this.httpClient.get<any>(`${this.baseApi}/lihat-user`).pipe(
  // mapping data baru sesuai yg dimap
  //     map((val) => {
  //       return val.data;
  //     })
  //   );
  // }
}
