import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  // username = '';
  // password = '';
  // showAlert = 'alert alert-info';

  formLogin: FormGroup;

  error = '';

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.formLogin = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(11)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  get errorControl() {
    return this.formLogin.controls;
  }

  doLogin() {
    console.log(this.formLogin);
    const payload = {
      noHP: this.formLogin.value.username,
      password: this.formLogin.value.password,
    };
    this.authService.login(payload).subscribe(
      (response) => {
        console.log(response);
        // if (response.token) {
        //   this.router.navigate(['/playground']);
        // }
        // alert(response.data.jwtrole);

        if (response.data.role === 'ROLE_ADMIN') {
          localStorage.setItem('token', response.data.token);
          this.router.navigate(['/admin/dashboard']);
          Swal.fire({
            title: 'Successfully Login!',
            text:
              'You have successfully login  as ' + response.data.role + '! 🥳',
            icon: 'success',
            confirmButtonColor: '#0046e6',
            confirmButtonText: 'OK',
          });
          localStorage.setItem('token', response.data.token);
        }
        if (response.data.role === 'ROLE_USER') {
          // this.router.navigate(['/users']);
          Swal.fire({
            title: 'Oops',
            text: 'You dont have permission to enter this page!',
            icon: 'error',
            confirmButtonColor: '#0046e6',
            confirmButtonText: 'OK',
          });
        }
      },
      (error) => {
        console.log(error);
        Swal.fire({
          title: 'Oops',
          text: 'You have entered wrong password or number phone!',
          icon: 'error',
          confirmButtonColor: '#0046e6',
          confirmButtonText: 'OK',
        });
        this.error = error.error.message;
      }
    );
  }
}
