import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent {
  // username = '';
  // password = '';
  // nama = '';
  // email = '';
  // tanggalLahir = '';
  // role = '';

  formRegister: FormGroup;

  error = '';

  // constructor(private authService: AuthService) {}

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.formRegister = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(11)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]],
      nama: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      tanggalLahir: ['', [Validators.required]],
      role: ['', [Validators.required]],
    });
  }

  get confirmPassword() {
    return (
      this.formRegister.value.password ==
      this.formRegister.value.confirmPassword
    );
  }

  get errorControl() {
    return this.formRegister.controls;
  }

  doRegister() {
    const payload = {
      noHP: this.formRegister.value.username,
      password: this.formRegister.value.password,
      nama: this.formRegister.value.nama,
      email: this.formRegister.value.email,
      tanggalLahir: this.formRegister.value.tanggalLahir,
      role: this.formRegister.value.role,
    };
    this.authService.register(payload).subscribe((response) => {
      console.log(response);
      // if (response.token) {
      //   this.router.navigate(['/playground']);
      // }
      // alert(response.jwtrole);
      // if (response.jwtrole === 'admin') {
      //   this.router.navigate(['/admin']);
      // }
      // if (response.jwtrole === 'user') {
      //   this.router.navigate(['/users']);
      // } else {
      //   this.showAlert = 'alert alert-warning';
      //   console.log("You don't have permission to enter this page");
      // }
    });
  }
}
