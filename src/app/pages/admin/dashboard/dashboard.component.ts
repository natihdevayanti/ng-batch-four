import { Component, ElementRef, OnInit } from '@angular/core';
import { UserList } from 'src/app/interfaces/user.interface';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  constructor(
    private elementRef: ElementRef,
    private usersService: UsersService
  ) {
    this.usersService
      .listUsers()
      .pipe()
      .subscribe((response) => {
        console.log(response);
        this.users = response;
      });
  }
  users!: UserList[];

  ngOnInit(): void {
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = '../assets/js/main.js';
    this.elementRef.nativeElement.appendChild(s);
  }
}
