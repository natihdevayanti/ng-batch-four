import { Component, TemplateRef } from '@angular/core';
import { UserList } from 'src/app/interfaces/user.interface';
import { UsersService } from 'src/app/services/users.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { map, Subject, switchMap } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
// import { Pipe, PipeTransform } from '@angular/core';
// import 'lodash-es/lodash';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent {
  users!: UserList[];
  modalRef?: BsModalRef;

  image!: string;
  imageFile!: File;
  formAdd: FormGroup;
  refresh = new Subject<void>();

  userDetail: UserList = {
    id: 0,
    nama: '',
    email: '',
    noHP: '',
    tanggalLahir: '',
    role: '',
    photo: '',
  };

  loadData() {
    this.userServices.listUsers().subscribe((response) => {
      console.log(response);
      this.users = response;
    });
  }

  constructor(
    private userServices: UsersService,
    private modalService: BsModalService,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {
    this.loadData();
    this.formAdd = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(11)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      nama: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      tanggalLahir: ['', [Validators.required]],
      role: ['admin'],
      photo: ['', [Validators.required]],
    });
  }

  get errorControl() {
    return this.formAdd.controls;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    this.doCheckDetail;
  }

  showPreview(event: any) {
    if (event) {
      const file = event.target.files[0];
      this.imageFile = file;
      const reader = new FileReader();
      reader.onload = () => {
        this.image = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  doAddUser() {
    Swal.fire({
      title: 'Successfully Add The Data!',
      text: 'You have successfully add 1 user data 🥳',
      icon: 'success',
      confirmButtonColor: '#0046e6',
      confirmButtonText: 'OK',
    });

    this.userServices
      .uploadPhoto(this.imageFile)
      .pipe(
        switchMap((val) => {
          const payload = {
            noHP: this.formAdd.value.username,
            password: this.formAdd.value.password,
            nama: this.formAdd.value.nama,
            email: this.formAdd.value.email,
            tanggalLahir: this.formAdd.value.tanggalLahir,
            role: this.formAdd.value.role,
            photo: val.data,
          };
          return this.authService.register(payload).pipe(map((val) => val));
        })
      )
      .subscribe((response) => {
        console.log(response);
        this.loadData();
      });
  }

  doCheckDetail(data: UserList) {
    console.log(data);
    console.log(data.photo);
    this.userDetail = data;
  }
}
