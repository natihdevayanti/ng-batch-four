import { Component, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { map, Subject, switchMap, takeUntil } from 'rxjs';
import { KategoriList } from 'src/app/interfaces/kategori.interface';
import { KategoriService } from 'src/app/services/kategori.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-kategori',
  templateUrl: './kategori.component.html',
  styleUrls: ['./kategori.component.scss'],
})
export class KategoriComponent {
  kategori!: KategoriList[];
  formAdd: FormGroup;
  formEdit: FormGroup;
  modalRef?: BsModalRef;

  image!: string;
  imageFile!: File | null;

  // imageFile!: File | null;
  refresh = new Subject<void>();

  kategoriDetail: KategoriList = {
    id: 0,
    namaKategori: '',
    gambarKategori: '',
  };

  constructor(
    private kategoriServices: KategoriService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder
  ) {
    this.loadData();
    this.formAdd = this.formBuilder.group({
      namaKategori: ['', [Validators.required]],
      gambarKategori: ['', [Validators.required]],
    });
    this.formEdit = this.formBuilder.group({
      namaKategori: [''],
      gambarKategori: [''],
    });
  }

  loadData() {
    this.kategoriServices
      .listKategori()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log(response);
        this.kategori = response;
      });
  }

  get errorControl() {
    return this.formAdd.controls;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  showPreview(event: any) {
    if (event) {
      const file = event.target.files[0];
      this.imageFile = file;
      const reader = new FileReader();
      reader.onload = () => {
        this.image = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  doAddKategori() {
    if (this.imageFile) {
      Swal.fire({
        title: 'Successfully Add The Data!',
        text: 'You have successfully add 1 kategori data 🥳',
        icon: 'success',
        confirmButtonColor: '#0046e6',
        confirmButtonText: 'OK',
      });
      this.kategoriServices
        .uploadPhotoKategori(this.imageFile)
        .pipe(
          switchMap((val) => {
            const payload = {
              namaKategori: this.formAdd.value.namaKategori,
              gambarKategori: val.data,
            };
            this.imageFile = null;
            return this.kategoriServices
              .addKategori(payload)
              .pipe(map((val) => val));
          })
        )
        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    }
  }

  doEditKategori() {
    if (this.imageFile) {
      Swal.fire({
        title: 'Successfully Edit The Data!',
        text: 'You have successfully edit kategori data 🥳',
        icon: 'success',
        confirmButtonColor: '#0046e6',
        confirmButtonText: 'OK',
      });

      this.kategoriServices
        .uploadPhotoKategori(this.imageFile)
        .pipe(
          switchMap((val) => {
            const payload = {
              namaKategori: this.formEdit.value.namaKategori,
              gambarKategori: val.data,
            };
            this.imageFile = null;
            this.image = '';
            return this.kategoriServices
              .editKategori(payload, this.kategoriDetail.id)
              .pipe(map((val) => val));
          })
        )
        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    } else {
      Swal.fire({
        title: 'Successfully Edit The Data!',
        text: 'You have successfully edit kategori data 🥳',
        icon: 'success',
        confirmButtonColor: '#0046e6',
        confirmButtonText: 'OK',
      });
      const payload = {
        namaKategori: this.formEdit.value.namaKategori,
        gambarKategori:
          this.formEdit.value.gambarKategori ??
          this.kategoriDetail.gambarKategori,
      };
      this.kategoriServices
        .editKategori(payload, this.kategoriDetail.id)
        .pipe(map((val) => val))

        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    }
  }

  doDeleteKategori() {
    if (this.imageFile) {
      Swal.fire({
        title: 'Successfully Delete The Data!',
        text: 'You have successfully delete kategori data 🥳',
        icon: 'success',
        confirmButtonColor: '#0046e6',
        confirmButtonText: 'OK',
      });

      this.kategoriServices
        .uploadPhotoKategori(this.imageFile)
        .pipe(
          switchMap((val) => {
            const payload = {
              namaKategori: this.formEdit.value.namaKategori,
              gambarKategori: val.data,
            };
            this.imageFile = null;
            return this.kategoriServices
              .deleteKategori(payload, this.kategoriDetail.id)
              .pipe(map((val) => val));
          })
        )
        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    } else {
      Swal.fire({
        title: 'Successfully Delete The Data!',
        text: 'You have successfully edit kategori data 🥳',
        icon: 'success',
        confirmButtonColor: '#0046e6',
        confirmButtonText: 'OK',
      });
      const payload = {
        namaKategori: this.formEdit.value.namaKategori,
        gambarKategori: this.kategoriDetail.gambarKategori,
      };
      this.kategoriServices
        .deleteKategori(payload, this.kategoriDetail.id)
        .pipe(map((val) => val))

        .subscribe((response) => {
          console.log(response);
          this.loadData();
        });
    }
  }

  doEditModal(data: KategoriList) {
    console.log(data);
    this.kategoriDetail = data;
    this.image = data.gambarKategori ?? '';
    this.formEdit.controls['namaKategori'].patchValue(data.namaKategori);
    this.formEdit.controls['gambarKategori'].patchValue(data.gambarKategori);
  }

  doDeleteModal(data: KategoriList) {
    console.log(data);
    this.kategoriDetail = data;
  }
}
