import { Component } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss'],
})
export class PlaygroundComponent {
  title = 'ng-batch-four';

  name = 'Natih';
  age = 20;
  status = false;

  showData = false;

  nomor = 6;

  person = {
    title: 'Test A',
    name: 'ngroki',
    age: 0,
    status: true,
  };

  personList = [
    {
      title: 'Test A',
      name: 'ngroki',
      age: 0,
      status: true,
    },
    {
      title: 'Test B',
      name: 'ngroki',
      age: 0,
      status: false,
    },
  ];

  datas = [1, 2, 3];
  // datas = new Array(10);

  constructor() {
    this.name = 'Roki';
    this.age = 21;
  }

  onCallBack(ev: any) {
    console.log(ev);
    this.personList.push(ev.data);
  }

  //   user: User;

  //   constructor(private authenticationService: AuthenticationService) {
  //       this.authenticationService.user.subscribe(x => this.user = x);
  //   }

  //   get isAdmin() {
  //     return this.user && this.user.role === Role.Admin;
  // }
}
